tolua (5.2.4-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release.
  * Switch to dpkg-source 3.0 (quilt) format (Closes: #1007375)
  * d/control: Point VCS-* to salsa, alioth is down since years.
    (Closes: #1018722)
  * Remove old compat level file and replace it with B-D on debhelper-compat,
     bumping from level 7 to 13. (Closes: #1018721)
  * changes required to bump S-V to 4.6.1. (Closes: #1018723)
    - Using https instead of http in several places.
    - Specifying Rules-Requre-Root.
  * Add missing CFLAGS to Makefile, for better hardening.

 -- Tobias Frost <tobi@debian.org>  Mon, 29 Aug 2022 18:22:36 +0200

tolua (5.2.0-1) unstable; urgency=low

  * New upstream version 5.2.0
  * Bump Standards-Version to 3.9.3.0. No changes necessary.
  * Added Vcs-* fields for newly published git repo
  * Specified source package format 1.0. This package is too simple to
    warrant conversion right now.
  * Documented development workflow in README.source
  * Bump Lua (build-)dependency to liblua5.2-dev
  * Update include path to /usr/include/lua5.2
  * Update linker flag -llua5.1->llua5.2
  * Add hardening support via dpkg-buildflags, also subsuming -g
  * Add 'test' make target alias to satisfy dh_auto_test

 -- Jimmy Kaplowitz <jimmy@debian.org>  Thu, 28 Jun 2012 00:41:41 -0400

tolua (5.1.3-1) unstable; urgency=low

  * Adopted usage of git-buildpackage
  * Switched debian/watch to use HTTP since upstream's FTP no longer
    works
  * New upstream version 5.1.3
  * Updated Standards-Version to 3.9.1.0. No changes necessary.

 -- Jimmy Kaplowitz <jimmy@debian.org>  Mon, 23 Aug 2010 02:45:45 -0400

tolua (5.1.2-1) unstable; urgency=low

  * New upstream release
  * Switched to debhelper 7 with minimal rules file format
  * Made clean target a little more complete

 -- Jimmy Kaplowitz <jimmy@debian.org>  Thu, 16 Jul 2009 06:01:05 +0200

tolua (5.1b-3) unstable; urgency=low

  * Converted debian/watch to format 3 (fixes tolua DEHS page)
  * Removed maintainer scripts, none of which did anything
  * Install MANIFEST file as documentation along with README, and
    reference it from debian/copyright as containing the date and time
    of upstream publication

 -- Jimmy Kaplowitz <jimmy@debian.org>  Mon, 28 Apr 2008 01:42:42 -0400

tolua (5.1b-2) unstable; urgency=low

  * Fixed missing quotes in make invocation in debian/rules (Closes:
    #476062)
  * Turned on -D_REENTRANT as per Lenny RC policy
  * Moved the setting of -g from config to debian/rules

 -- Jimmy Kaplowitz <jimmy@debian.org>  Mon, 14 Apr 2008 18:20:54 -0400

tolua (5.1b-1) unstable; urgency=low

  * New upstream release (Closes: #288324)
  * Dropped shared library package libtolua0; upstream seems not to have
    added a shared library despite acknowledging my shared library patch
    well before releasing the current version
  * Updated to current format of debian/watch file
  * Added support for noopt DEB_BUILD_OPTIONS keyword
  * Properly removed static library in make clean target even though
    upstream Makefiles don't
  * Updated to Standards-Version 3.7.3.0
  * Removed references to dh_undocumented
  * Switched from DH_COMPAT variable to debian/compat file
  * Unignored errors from $(MAKE) clean in debian/rules
  * Added override for spurious lintian warning about copyright notice
  * Fixed section to match Debian override (devel -> libdevel)

 -- Jimmy Kaplowitz <jimmy@debian.org>  Sun, 17 Feb 2008 00:47:16 -0500

tolua (4.0a-3) unstable; urgency=low

  * Changed -Wl,-shared to -shared in src/lib/Makefile to fix building
    on hppa (Closes: #155688)

 -- Jimmy Kaplowitz <jimmy@debian.org>  Wed, 07 Aug 2002 01:38:44 -0400

tolua (4.0a-2) unstable; urgency=low

  * Add a runtime library package. (Closes: #148721)
  * Do add an extra dependency as per #146983 ;-)
  * Move the tolua binary to the -dev package.
  * Rename the binary packages to libtolua*; set a Conflicts: in the -dev
    package.
  * Write a small manpage.
  * Bump debhelper build-depends and set DH_COMPAT=4, to be able to use the new
    dh_makeshlibs behaviour. Use dh_install instead of dh_movefiles.
  * All of the above changes are due to Filip Van Raemdonck. Thanks very much.
  * Expand Filip's man page by adapting tolua's built-in help output.
  * Add Provides: and Replaces: to the -dev package as well, to ensure that
    upgrades work as expected.
  * Make the libtolua-dev package depend on the same version of the libtolua0
    package.

 -- Jimmy Kaplowitz <jimmy@debian.org>  Mon,  5 Aug 2002 03:26:18 -0400

tolua (4.0a-1) unstable; urgency=low

  * Initial Release.

 -- Jimmy Kaplowitz <jimmy@debian.org>  Tue, 30 Apr 2002 01:37:09 -0400
